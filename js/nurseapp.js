window.onload = inicializar;
var formPaciente;
var refRegistro;
var tbodyTablaPacientes;
var refRegistroAEditar;
var CREATE ="Anadir Convalidacion";
var UPDATE ="Modificar Convalidacion";
var modo = CREATE;


function inicializar(){
  formPaciente = document.getElementById("form-paciente");
  formPaciente.addEventListener("submit", enviarRegistroFirebase, false);
  refRegistro = firebase.database().ref().child("registro");
  tbodyTablaPacientes = document.getElementById("tbody-tabla-pacientes");
  mostrarRegistroFirebase();
}

function enviarRegistroFirebase(event){
  event.preventDefault();
  switch (modo) {
    case CREATE:
    refRegistro.push({
       nombrePaciente     : event.target.nombrePaciente.value,
       edadPaciente       : event.target.edadPaciente.value,
       presionPaciente    : event.target.presionPaciente.value,
       frecuenciaPaciente : event.target.frecuenciaPaciente.value
   });
   break;
   case UPDATE:
    refRegistroAEditar.update({
      nombrePaciente     : event.target.nombrePaciente.value,
      edadPaciente       : event.target.edadPaciente.value,
      presionPaciente    : event.target.presionPaciente.value,
      frecuenciaPaciente : event.target.frecuenciaPaciente.value
  });
  document.getElementById("boton-enviar-registro").value = CREATE;
  modo=CREATE;
  default:
  }
   formPaciente.reset();
}


function mostrarRegistroFirebase(){
  refRegistro.on("value", function(snap){
    var datos = snap.val();
    var filasAMostrar = "";
    for(var key in datos){
      filasAMostrar += "<tr>" +
                       "<td>" + datos[key].nombrePaciente + "</td>" +
                       "<td>" + datos[key].edadPaciente + "</td>" +
                       "<td>" + datos[key].presionPaciente + "</td>" +
                       "<td>" + datos[key].frecuenciaPaciente + "</td>" +
                       '<td>' +
                          '<a href="#registro" class="btn btn-outline-success editar" data-convalidacion="' + key + '">'+
                          'Editar' +
                          '</a>'+
                        '</td>'+
                        '<td>' +
                         '<button  class="btn btn-outline-secondary borrar"  data-convalidacion="' + key + '">'+
                         'Eliminar' +
                         '</button>'
                       '</td>'+
                       "</tr>";
    }
      tbodyTablaPacientes.innerHTML=filasAMostrar;
      if (filasAMostrar != ""){
        var elementosEditables = document.getElementsByClassName("editar");
        for(var i= 0; i< elementosEditables.length; i++){
          elementosEditables[i].addEventListener("click",editarRegistroDeFirebase,false);
        }
        var elementosBorrables = document.getElementsByClassName("borrar");
        for(var i= 0; i< elementosBorrables.length; i++){
          elementosBorrables[i].addEventListener("click",borrarRegistroDeFirebase,false);
        }
      }
  });
}

function borrarRegistroDeFirebase(){
 var keyDeRegistroBorrar = this.getAttribute("data-convalidacion");
 var refRegistroBorrar = refRegistro.child(keyDeRegistroBorrar);
 refRegistroBorrar.remove();
}


function editarRegistroDeFirebase(){
  var keyDeRegistroAEditar = this.getAttribute("data-convalidacion");
  refRegistroAEditar = refRegistro.child(keyDeRegistroAEditar);
  refRegistroAEditar.once("value", function(snap){
    var datos = snap.val();
    document.getElementById("nombre-paciente").value = datos.nombrePaciente;
    document.getElementById("edad-paciente").value = datos.edadPaciente;
    document.getElementById("presion-paciente").value = datos.presionPaciente;
    document.getElementById("frecuencia-paciente").value = datos.frecuenciaPaciente;
  });
  document.getElementById("boton-enviar-registro").value = UPDATE;
  modo = UPDATE;
}
